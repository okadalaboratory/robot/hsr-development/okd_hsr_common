#!/usr/bin/env python3
# Copyright (C) 2016 Toyota Motor Corporation
# Copyright (C) 2022 Hiroyuki Okada
## coding: UTF-8
import math
import rospy
import actionlib
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import Point, PoseStamped, Quaternion
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import tf.transformations

def main():
    # アクションクライアントの初期化
    cli = actionlib.SimpleActionClient('/move_base/move', MoveBaseAction)
    # アクションサーバとの接続が確立するまで待つ
    cli.wait_for_server()

    # 移動先の位置、姿勢(x座標、y座標、回転)
    goal_x, goal_y, goal_yaw = 1.0, 1.0, math.radians(90)

    pose = PoseStamped()
    pose.header.stamp = rospy.Time.now()
    pose.header.frame_id = "map"
    pose.pose.position = Point(goal_x, goal_y, 0)
    quat = tf.transformations.quaternion_from_euler(0, 0, goal_yaw)
    pose.pose.orientation = Quaternion(*quat)
    goal = MoveBaseGoal()
    goal.target_pose = pose

    # アクションサーバにメッセージを送信
    cli.send_goal(goal)
    # 命令が完了するまで待つ
    cli.wait_for_result()

    # 結果の表示
    action_state = cli.get_state()
    if action_state == GoalStatus.SUCCEEDED:
        rospy.loginfo("Navigation Succeeded.")
    else:
        rospy.loginfo("Navigation Failed.")

if __name__ == "__main__":
    rospy.init_node('navigation_actionlib')
    main()





