#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php


"""
upower コマンドを使って，背中に積んだPCのバッテリー残量を求める
$ upower -i /org/freedesktop/UPower/devices/battery_BAT0 
  native-path:          BAT0
  vendor:               SMP
  model:                01YU911
  serial:               1549
  power supply:         yes
  updated:              2023年07月27日 08時42分08秒 (79 seconds ago)
  has history:          yes
  has statistics:       yes
  battery
    present:             yes
    rechargeable:        yes
    state:               charging
    warning-level:       none
    energy:              60.88 Wh
    energy-empty:        0 Wh
    energy-full:         68.36 Wh
    energy-full-design:  80.4 Wh
    energy-rate:         15.45 W
    voltage:             17.36 V
    time to full:        29.0 minutes
    percentage:          89%
    capacity:            85.0249%
    technology:          lithium-polymer
    icon-name:          'battery-full-charging-symbolic'
  History (charge):
    1690414928 89.000 charging
  History (rate):
    1690414928 15.450 charging

rshを使うことでリモートマシンのバッテリーの状態を取得する

sshpassによりパスワード付きでリモートマシンにアクセスできる





"""
import sys
import os
import datetime
import math
import rospy
from std_msgs.msg import *
from tmc_msgs.msg import *
import hsrb_interface
from hsrb_interface import geometry
import subprocess
import re

REMOTE_PC="pc1.local"

# バッテリーの残量を取得
def getBatteryPercentage():
    res = subprocess.run(
        "sshpass -p tamagawa rsh roboworks@pc1.local upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e percentage |awk '{print $2}'",
        capture_output=True,
        text=True,
        shell=True
    )
    percentage=0
    if res.returncode==0: 
        result = res.stdout.replace('%', '')
        percentage = int(result)
        rospy.loginfo("Battery Percentage(%s): %d",REMOTE_PC,percentage)
    else:
        rospy.loginfo("can not get charge info.(%s)", REMOTE_PC)

    return percentage


# バッテリーが充電中か否か
def getChargingState():
    res = subprocess.run(
        "sshpass -p tamagawa rsh roboworks@pc1.local upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -e state |awk '{print $2}'",
        capture_output=True,
        text=True,
        shell=True
    )

    if res.returncode==0: # リターンコード
        if res.stdout.startswith("charging"):
            rospy.loginfo("charging(%s)", REMOTE_PC)
            state = 1
        elif res.stdout.startswith("fully-charged"):
            rospy.loginfo("fully-charged(%s)", REMOTE_PC)
            state = 2
        elif res.stdout.startswith("pending-charge"):
            rospy.loginfo("pending-charged(%s)", REMOTE_PC)
            state = 3
        elif res.stdout.startswith("discharging"):
            rospy.loginfo("discharging(%s)", REMOTE_PC)
            state = 0
        else:
            rospy.loginfo("can not get charging state(%s)", REMOTE_PC)
            state = -1
    else:
        rospy.loginfo("can not get charge info.(%s)", REMOTE_PC)
        state = 99

    return state
 

def main():
    rospy.init_node('basck_checkbattery', anonymous=True)
    rospy.loginfo("Back PC check battery status")  

    robot = hsrb_interface.Robot()
    tts = robot.get('default_tts')
    rospy.sleep(1)
    

    # バッテリーの残量
    charge=getBatteryPercentage()
    # Detect robot's language
    if os.environ['LANG'] == 'ja_JP.UTF-8':
        rospy.loginfo(u"背中のPCのバッテリーの残量は" + str(int(charge)) +"パーセントです")
        tts.say(u"背中のPCのバッテリーの残量は" + str(int(charge)) +"パーセントです")
    else:
        rospy.loginfo(u"Battery level is " + str(int(charge)) +u" percent")
        tts.say(u"Battery level is" + str(int(charge)) +u"percent")
    rospy.sleep(3)


    # 充電中か否か
    state=getChargingState()
    if state == 0:
        tts.say(u"背中のPCを充電してください")
    rospy.sleep(1)


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass

