#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import smach
import smach_ros
import tf2_ros
import math
import numpy as np
import random
import traceback

import hsrb_interface

from tf.transformations import quaternion_matrix, quaternion_from_euler, quaternion_from_matrix

from hsrb_interface import geometry
from geometry_msgs.msg import Vector3Stamped, Pose, PoseStamped, PointStamped
from geometry_msgs.msg import WrenchStamped, Twist

from common import speech
from common.smach_states import *
from common import rospose_to_tmcpose
from common.smach_states import ReadQRCode
##################################################

# Main
robot = hsrb_interface.Robot()

whole_body = robot.get("whole_body")
omni_base = robot.get("omni_base")
gripper = robot.get('gripper')
tf_buffer = robot._get_tf2_buffer()

default_tts = speech.DefaultTTS()
console = speech.Console()

SAY = default_tts.say

whole_body.move_to_neutral()
rospy.loginfo('initializing...')
rospy.sleep(3)

##Speech
import common.hokuto_gogetit as ARENA

from pocketsphinx_jsgf import PocketSphinxClient
sphinx = PocketSphinxClient()

def listen_location(timeout):
    mylocations = list(ARENA.LOCATIONS.keys())
    rule = mylocations + [' place '+ loc for loc in mylocations]
    sphinx.set_single_rule(rule)
    return sphinx.next_speech(timeout)

def parse_location(text):
    for mylocation in ARENA.LOCATIONS.keys():
        if mylocation in text:
            return mylocation, ARENA.LOCATIONS[mylocation]
    return None

def parse_locations(text):
    locs = []
    for mylocation in ARENA.LOCATIONS.keys():
        if mylocation in text:
            locs.append(mylocation)
    return locs

def create_sm():

    sm = smach.StateMachine(outcomes=['success', 'failure'])

    sm.userdata.places_names = []
    sm.userdata.place_names = []
    sm.userdata.qr_text = []
    
    with sm:
        ##########
        # START: BASIC QR FUNCTION
        ##########
#	#Confirms the spoken command
#        @smach.cb_interface(outcomes=['stop', 'continue', 'failure'])
#        def conf_yesno_cb(userdata):
#                try:
#                    SAY('Should I stop?')
#
#                    for _ in range(3):
#                        res = sphinx.next_yes_or_no()
#
#                        if res == 'yes':
#                            SAY('Bye.')
#                            return 'stop'
#
#                        if res == 'no':
#                            SAY('Ok.')
#                            return 'continue'
#
#                        SAY('Please say yes or no.')
#                    SAY('Oh, sorry.')
#                    return 'failure'
#                except:
#                    SAY('Oh, sorry.')
#                    return 'failure'
#        smach.StateMachine.add('YESNO', smach.CBState(conf_yesno_cb),
#                           transitions = {'stop': 'success',
#                                          'continue': 'YESNO',
#                                          'failure': 'failure'})
		                          
        # Speech recognition function
        @smach.cb_interface(outcomes=['success', 'failure'])
        def ask_location_cb(self):
            try:
                SAY('Where am I?')

                return 'success'
            except:
                return 'failure'

        smach.StateMachine.add('ASKLOCATION', smach.CBState(ask_location_cb),
                               transitions={'success': 'LISTENLOCATION', #'READQRCODE',  # 'LISTENLOCATION',
                                            'failure': 'failure'})

        #Listen command
        smach.StateMachine.add('LISTENLOCATION', GetCommand(listen_location,
                                                            parse_location,
                                                            timeout=30.,
                                                            say_fn=SAY,
                                                            prompt_msg="",
                                                            success_msg="",
                                                            retry=0),
                           transitions = {'success': 'CONFIRMLOCATION',
                                          'bypass': 'CONFIRMLOCATION',
                                          'timeout': 'ASKLOCATION',
                                          'failure': 'failure'},
                           remapping = {'command': 'place_text'})

        #Confirms the spoken command
        @smach.cb_interface(outcomes=['correct', 'incorrect', 'stop', 'failure'],
                            input_keys=['place_text', 'place_names'],
                            output_keys=['place_names'])
        def conf_loc_cb(userdata):
               try:
                   place, coord = parse_location(userdata.place_text)

                   if place == 'stop':
                       SAY('Should I stop?')
                   else:
                       SAY('The place is {}?'.format(place))
                   for _ in range(3):
                       res = sphinx.next_yes_or_no()

                       if res == 'yes':
                           if place == 'stop':
                               return 'stop'
                           else:
                               SAY('OK.')
                               userdata.place_names.append(place)
                               return 'correct'

                       if res == 'no':
                           SAY('Oh, sorry.')
                           return 'incorrect'

                       SAY('Please say yes or no.')
                   return 'failure'
               except:
                   import traceback
                   traceback.print_exc()
                   return 'failure'
        smach.StateMachine.add('CONFIRMLOCATION', smach.CBState(conf_loc_cb),
                               transitions = {'correct': 'LISTENLOCATION',
                                              'incorrect': 'LISTENLOCATION',
                                              'stop': 'STOPLOCATION',
                                              'failure': 'failure'})

        # Read qr code
        smach.StateMachine.add('READQRCODE', ReadQRCode(),
                               transitions={'success': 'CONFIRMLONGQRLOCATION', 'timeout': 'failure', 'failure': 'failure'})

        # Confirms long qr command
        @smach.cb_interface(outcomes=['correct', 'stop', 'failure'],
                            input_keys=['qr_text', 'place_names'],
                            output_keys=['place_names'])
        def conf_long_qr_loc_cb(userdata):
            try:
                txt = userdata.qr_text

                _stop = False
                if txt == 'stop':
                    SAY('I will stop.')
                    return 'stop'
                else:
                    places = [x.strip() for x in txt.split(',')]
                    for i in range(len(places)):
                        place = places[i]
                        if place == 'stop':
                            _stop = True
                        else:
                            SAY('The place is {}.'.format(place))
                            userdata.place_names.append(place)

                    if _stop == True:
                        return 'stop'

                    return 'correct'

                return 'failure'
            except:
                return 'failure'
        smach.StateMachine.add('CONFIRMLONGQRLOCATION', smach.CBState(conf_long_qr_loc_cb),
                               transitions={'correct': 'READQRCODE',
                                            'stop': 'STOPLOCATION',
                                            'failure': 'READQRCODE'})

        @smach.cb_interface(outcomes=['success', 'failure'],
                            input_keys=['place_names', 'places_names'],
                            output_keys=['place_names', 'places_names'])
        def stop_location_cb(userdata):
            try:
                SAY('I have learnt the place.')

                userdata.places_names.append(userdata.place_names)
                userdata.place_names = []
                rospy.loginfo('place_name: {}'.format(userdata.places_names))
                return 'success'
            except:
                return 'failure'

        smach.StateMachine.add('STOPLOCATION', smach.CBState(stop_location_cb),
                               transitions={'success': 'success',
                                            'failure': 'failure'})

        ##########
        # END: BASIC QR FUNCTION
        ##########

    return sm


sm = create_sm()
outcome = sm.execute()

if outcome == 'success':
    rospy.loginfo('I finished the task.')
else:
    rospy.signal_shutdown('Some error occured.')
