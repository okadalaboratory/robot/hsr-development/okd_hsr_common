#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import smach
import smach_ros
import tf2_ros
import math
import numpy as np
import random
import traceback

import hsrb_interface

from tf.transformations import quaternion_matrix, quaternion_from_euler, quaternion_from_matrix

from hsrb_interface import geometry
from geometry_msgs.msg import Vector3Stamped, Pose, PoseStamped, PointStamped
from geometry_msgs.msg import WrenchStamped, Twist

from common import speech
from common.smach_states import *
from common import rospose_to_tmcpose
##################################################

# Main
robot = hsrb_interface.Robot()

whole_body = robot.get("whole_body")
omni_base = robot.get("omni_base")
gripper = robot.get('gripper')
tf_buffer = robot._get_tf2_buffer()

default_tts = speech.DefaultTTS()
console = speech.Console()

SAY = default_tts.say

whole_body.move_to_neutral()
rospy.loginfo('initializing...')
rospy.sleep(3)


def create_sm():

    sm = smach.StateMachine(outcomes=['success', 'failure'])

    with sm:
        ##########
        # START: BASIC STATE FUNCTION
        ##########
        @smach.cb_interface(outcomes=['success', 'failure'])
        def test_state_cb(self):
            try:
                # INSERT YOUR CODE HERE!!
                # FOR EXAMPLE:

                # move to origin
                omni_base.go_abs(0.0, 0.0, 0.0)

                return 'success'
            except:
                return 'failure'

        smach.StateMachine.add('TESTSTATE', smach.CBState(test_state_cb),
                               transitions={'success': 'success',
                                            'failure': 'failure'})
        ##########
        # END: BASIC STATE FUNCTION
        ##########

    return sm


sm = create_sm()
outcome = sm.execute()

if outcome == 'success':
    rospy.loginfo('I finished the task.')
else:
    rospy.signal_shutdown('Some error occured.')
