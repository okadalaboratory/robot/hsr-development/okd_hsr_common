#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved

import rospy
import smach
import smach_ros
import tf2_ros

import math
from datetime import datetime
import time
import numpy as np
import random
import traceback
import types
from contextlib import ExitStack
import tf.transformations
from tf.transformations import quaternion_matrix, quaternion_from_euler, quaternion_from_matrix
from geometry_msgs.msg import Vector3Stamped, Pose, PoseStamped, PointStamped
from geometry_msgs.msg import WrenchStamped, Twist
import actionlib
from actionlib_msgs.msg import GoalStatus
from geometry_msgs.msg import Point, PoseStamped, Quaternion
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal

class DefaultTTS:
    def __init__(self):
        import actionlib
        from tmc_msgs.msg import TalkRequestAction
        self.client = actionlib.SimpleActionClient('/talk_request_action', TalkRequestAction)
        self.client.wait_for_server(rospy.Duration(1.))
        #from tmc_msgs.msg import Voice
        #self.talk_pub = rospy.Publisher('/talk_request', Voice, queue_size=10)

    def say(self, text, wait_result=True):
        from tmc_msgs.msg import TalkRequestGoal
        goal = TalkRequestGoal()
        goal.data.language = 1   # 0: Japanese, 1: English
        goal.data.sentence = text
        self.client.send_goal(goal)
        if wait_result:
            self.client.wait_for_result()
        #from tmc_msgs.msg import Voice
        #voice = Voice()
        #voice.language = 1
        #voice.sentence = text
        #self.talk_pub.publish(voice)




# for Pumas Navigation at TRCP DSPL package
from takeshi_tools.nav_tool_lib import nav_module

class Navigate(smach.State):
    '''
    actionlibを使って移動するためのSmaｃhステート
    途中でキャンセルも可能
    ※PumusNaviと異なり，　地図に無い障害物回避は実装されていない

    Navigate()
    '''
    def __init__(self, goal_pose, ref_frame='map', timeout=120., say_fn=None, start_msg="",timeout_msg="",success_msg="", contexts=[], spin_fn=None, input_keys=[], additional_outcomes=[], output_keys=[]):

        import actionlib
        from move_base_msgs.msg import MoveBaseAction
        smach.State.__init__(self,outcomes=['success', 'failure', 'timeout']+additional_outcomes,
                input_keys=input_keys,output_keys=output_keys)

        self.goal_pose = goal_pose
        self.ref_frame = ref_frame
        self.timeout = timeout
        self.start_msg = start_msg
        self.timeout_msg = timeout_msg
        self.success_msg = success_msg
        self.say_fn = say_fn if say_fn else DefaultTTS().say
        self.contexts = contexts
        if spin_fn is not None:
            self.spin_fn = spin_fn
        else:
            rate = rospy.Rate(1.)
            self.spin_fn = lambda ud: rate.sleep()
        self.client = actionlib.SimpleActionClient(
            '/move_base/move', MoveBaseAction)
        self.client.wait_for_server(rospy.Duration(1.))

    def execute(self, userdata):
        try:
            from move_base_msgs.msg import MoveBaseGoal
            import math
            import actionlib

            if isinstance(self.goal_pose, types.FunctionType):
                goal_pose = self.goal_pose(userdata)
            else:
                goal_pose = self.goal_pose
            if isinstance(self.ref_frame, types.FunctionType):
                ref_frame = self.ref_frame(userdata)
            else:
                ref_frame = self.ref_frame
            rospy.loginfo('Moving to {}, {}'.format(
                goal_pose[0], goal_pose[1]))

            if self.say_fn and self.start_msg:
                try:
                    msg = self.start_msg.format(**userdata)
                    self.say_fn(msg)
                except:
                    rospy.logerr(traceback.format_exc())

            goal = MoveBaseGoal()
            goal.target_pose.header.frame_id = ref_frame
            goal.target_pose.pose.position.x = goal_pose[0][0]
            goal.target_pose.pose.position.y = goal_pose[0][1]
            goal.target_pose.pose.orientation.z = math.sin(goal_pose[1]/2)
            goal.target_pose.pose.orientation.w = math.cos(goal_pose[1]/2)
            self.client.send_goal(goal)
            timeout = rospy.Duration(self.timeout) if self.timeout else None
            with ExitStack() as stack:
#                stack.enter_context(particle_sub)
#                stack.enter_context(sphinx_sub)
                if self.contexts:
                    for i in self.contexts:
                        stack.enter_context(i)
                while not rospy.is_shutdown():
                    state = self.client.get_state()
                    rospy.loginfo('MoveBaseAction state: ' +
                                  actionlib.GoalStatus.to_string(state))
                    if state != actionlib.GoalStatus.PENDING and state != actionlib.GoalStatus.ACTIVE:
                        break
                    outcome = self.spin_fn(userdata)
                    if isinstance(outcome, str):
                        self.client.cancel_goal()
                        return outcome

            if self.client.get_state() == actionlib.GoalStatus.SUCCEEDED:
                if self.say_fn and self.success_msg:
                    try:
                        msg = self.success_msg.format(**userdata)
                        self.say_fn(msg)
                    except:
                        rospy.logerr(traceback.format_exc())
                return 'success'
            if self.say_fn and self.timeout_msg:
                try:
                    msg = self.timeout_msg.format(**userdata)
                    self.say_fn(msg)
                except:
                    rospy.logerr(traceback.format_exc())
            self.client.cancel_goal()
            return 'timeout'
        except:
            rospy.logerr(traceback.format_exc())
            return 'failure'

class Navi(smach.State):
    '''
    hsrb_interfaceを使って移動するためのSmaｃhステート
    PumusNaviは地図に無い障害物を回避できる
    '''
    def __init__(self, goal_pose, ref_frame='map', timeout=120., say_fn=None, start_msg="",timeout_msg="",success_msg="", contexts=[], spin_fn=None, input_keys=[], additional_outcomes=[], output_keys=[]):

        smach.State.__init__(self,outcomes=['success', 'failure', 'timeout']+additional_outcomes,
                input_keys=input_keys,output_keys=output_keys)

        import hsrb_interface
        from hsrb_interface import geometry
        robot = hsrb_interface.Robot()

        whole_body = robot.get("whole_body")
#        self.omni_base = robot.get("omni_base")
#        self.omni_base = nav_module("hsrb")  # New initalisation (Pumas)
        self.omni_base = nav_module("pumas")  # New initalisation (Pumas)
        self.goal_pose = goal_pose
        self.ref_frame = ref_frame
        self.timeout = timeout
        self.start_msg = start_msg
        self.timeout_msg = timeout_msg
        self.success_msg = success_msg
        self.say_fn = say_fn if say_fn else DefaultTTS().say
        self.contexts = contexts
        if spin_fn is not None:
            self.spin_fn = spin_fn
        else:
            rate = rospy.Rate(1.)
            self.spin_fn = lambda ud: rate.sleep()

    def execute(self, userdata):
        from move_base_msgs.msg import MoveBaseGoal
        import math
        import actionlib
        import hsrb_interface
        if isinstance(self.goal_pose, types.FunctionType):
            goal_pose = self.goal_pose(userdata)
        else:
            goal_pose = self.goal_pose
        if isinstance(self.ref_frame, types.FunctionType):
            ref_frame = self.ref_frame(userdata)
        else:
            ref_frame = self.ref_frame
        rospy.loginfo('Moving to {}, {}'.format(
        goal_pose[0], goal_pose[1]))

        try:
            self.omni_base.go_abs(goal_pose[0][0], goal_pose[0][1], goal_pose[1], self.timeout)
            return 'success'
        except hsrb_interface.exceptions.MobileBaseError as e:
            print('catch MobileBaseError:', e)
            rospy.logerr(traceback.format_exc())
            return 'timeout'
        except:
            print('omni_base.go_abs Error')
            return 'failure'



def main():
    # TOYOTA HSR interface

    import hsrb_interface
    from hsrb_interface import geometry
    robot = hsrb_interface.Robot()
    '''
    default_tts = robot.try_get('default_tts')
    #default_tts = DefaultTTS()
    SAY = default_tts.say
    '''    
    #発話のテスト
    '''
    SAY("Hello this is test")
    rospy.sleep(1)
    default_tts = DefaultTTS()
    SAY = default_tts.say
    SAY("Hello this is another test")
    '''

    whole_body = robot.get("whole_body")
    whole_body.move_to_neutral()
    rospy.loginfo('initializing...')
    rospy.sleep(1)

    def create_sm():
        sm = smach.StateMachine(outcomes=['success', 'failure', 'timeout'])
        with sm:



            smach.StateMachine.add('NaviState',Navi(lambda ud: ((1.0, 0.0), math.radians(0)),timeout=120),  
           # smach.StateMachine.add('NaviState',Navigate(lambda ud: ((1.0, 1.0), math.radians(180)　)),  
           transitions = {'success':'success','failure':'failure','timeout':'timeout'})

        ##########
	    #END: BASIC STATE FUNCTION
	    ##########
        return sm

    sm = create_sm()
    outcome = sm.execute()
    if outcome == 'success':
        rospy.loginfo('I finished the task.')
    elif outcome == 'timeout':
        rospy.loginfo('timeout!!')
    else:
        rospy.signal_shutdown('Some error occured.')

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException: pass




