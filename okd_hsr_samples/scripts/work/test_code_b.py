#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import rospy
import smach
import smach_ros
import tf2_ros
import math
import numpy as np
import random
import traceback

import hsrb_interface

from tf.transformations import quaternion_matrix, quaternion_from_euler, quaternion_from_matrix

from hsrb_interface import geometry
from geometry_msgs.msg import Vector3Stamped, Pose, PoseStamped, PointStamped
from geometry_msgs.msg import WrenchStamped, Twist

from common import speech
from common.smach_states import *
from common import rospose_to_tmcpose

##################################################

# Main
robot = hsrb_interface.Robot()

whole_body = robot.get("whole_body")
omni_base = robot.get("omni_base")
gripper = robot.get('gripper')
tf_buffer = robot._get_tf2_buffer()

default_tts = speech.DefaultTTS()
console = speech.Console()

SAY = default_tts.say

whole_body.move_to_neutral()
rospy.loginfo('initializing...')
rospy.sleep(3)

def create_sm():

  sm = smach.StateMachine(outcomes=['success', 'failure'])

  with sm:
    smach.StateMachine.add('WAIT_HAND', WaitHandPushed(timeout=120., say_fn=SAY, prompt_msg="Push the hand to start", success_msg=""), transitions={'success': 'FIRSTSTATE', 'timeout': 'FIRSTSTATE', 'failure': 'failure'})
    @smach.cb_interface(outcomes=['success', 'next', 'failure'])
    def first_state_cb(self):
      try:
        omni_base.go_abs(0.0, 0.0, 0.0)
        next = random.randint(0,1)
        rospy.loginfo('next = {}'.format(next))

        if next == 1:
          return 'next'
        else:
          return 'success'
      except:
        return 'failure'

    smach.StateMachine.add('FIRSTSTATE', smach.CBState(first_state_cb),
				transitions = {'success': 'success',
					       'next': 'NEXTSTATE',
					       'failure': 'failure'})


    @smach.cb_interface(outcomes=['success', 'failure'])
    def next_state_cb(self):
      try:
        whole_body.move_to_go()
	return 'success'
      except:
        return 'failure'

    smach.StateMachine.add('NEXTSTATE', smach.CBState(next_state_cb),
				transitions = {'success': 'success',
					       'failure': 'failure'})

  return sm

sm = create_sm()

outcome = sm.execute()

if outcome == 'success':
	SAY('I finished the task.')
	#rospy.sleep(2)
	rospy.loginfo('I finished the task.')
else:
	SAY('Sorry, something happend. I did not finish the task.')
	rospy.signal_shutdown('Some error occured.')

