#!/usr/bin/env python3
# Copyright (C) 2022 Hiroyuki Okada
## coding: UTF-8
import rospy
import smach
import smach_ros
import tf2_ros
import math
import numpy as np
import random
import traceback

class State1(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['done','exit'])
        self.counter = 0

    def execute(self, userdata):
        rospy.loginfo('Executing state STATE1')
        rospy.sleep(2.0)
        if self.counter < 3:
            self.counter += 1
            return 'done'
        else:
            return 'exit'

class State2(smach.State):
    def __init__(self):
        smach.State.__init__(self, outcomes=['done'])

    def execute(self, userdata):
        rospy.loginfo('Executing state START2')
        rospy.sleep(2.0)
        return 'done'

def main():
    sm_top = smach.StateMachine(outcomes=['succeeded'])
    with sm_top:
        smach.StateMachine.add('STATE1', State1(), transitions={'done':'STATE2', 'exit':'succeeded'})
        smach.StateMachine.add('STATE2', State2(), transitions={'done':'STATE1'})

    outcome = sm_top.execute()

    
if __name__ == "__main__":
    rospy.init_node('smach_sample_a')
    main()
