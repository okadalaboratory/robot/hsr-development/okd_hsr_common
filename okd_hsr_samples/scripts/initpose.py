#!/usr/bin/env python3
## coding: UTF-8

"""
ROS Navigationの初期位置・姿勢をpythonプログラムから設定する
initpose.py

HSR開発マニュアルには「2.1.3 自己位置の初期化」として、RVizを利用した方法が紹介されています。RVizの2D Pose EstimateツールをGUIから使うのは不便な時があります。

本プログラムではロボットの実際の位置とROS Navigation上での位置・姿勢をpythonプログラムから設定します。
部屋の決まった位置にロボットを置いて、使い始めるような場合に便利です。

使い方：
initpose.py _init_x:=1.0 _init_y:=1.2 _init_th:=0.5
initpose.py #引数を省略すると、全て0.0に設定される

参考：
[ROS Q&A] 200 - How to set the initial pose in ROS Navigation programatically
https://www.youtube.com/watch?v=-81TEdn6qLU

roscppのPublisherの挙動を調べる
http://ros-robot.blogspot.com/2011/04/
"""

import sys
import datetime
import math
from math import pi

import rospy
from std_msgs.msg import *
from geometry_msgs.msg import *
from geometry_msgs.msg import Point, PoseStamped, Quaternion
import tf

# ROSノードを起動
rospy.loginfo("Setting Initial Pose")  
rospy.init_node('initpose', anonymous=True)

# ROSパラメータで初期位置・姿勢を指定
init_x  = rospy.get_param('~init_x', 0.0) 
init_y  = rospy.get_param('~init_y', 0.0)
init_th = rospy.get_param('~init_th',0.0)
rospy.loginfo("(x, y, th)=(%f, %f, %f)",init_x,init_y,init_th)

pub = rospy.Publisher(
        '/laser_2d_correct_pose', 
        PoseWithCovarianceStamped,
        queue_size=100)
# Wait for connection
while self.pub.get_num_connections() == 0:
        rospy.sleep(0.1)

#rospy.sleep(1) 
#sleepがないと、そのスレッドが動く前にプログラムが終わってしまう

p = PoseWithCovarianceStamped();
msg = PoseWithCovariance();
q_angle = tf.transformations.quaternion_from_euler(
       0.0, 0.0, init_th , 'sxyz')
q = Quaternion(*q_angle)
msg.pose = Pose(Point(init_x ,init_y, 0.0), q);
msg.covariance = [0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.25, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06853];
p.pose = msg;
p.header.stamp = rospy.Time.now()
p.header.frame_id = 'map'

pub.publish(p)
rospy.sleep(1) #rospy.sleep(1) #sleepがないと、そのスレッドが動く前にプログラムが終わってしまう
