#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

#  * Redistributions of source code must retain the above copyright notice,
#  this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#  * Neither the name of Toyota Motor Corporation nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import rospy
import okd_hsr_lib
_SPEECH_SENTENCE1 = [u'こんにちは，私の名前はHSRです',
                      'hello my name is HSR']
_SPEECH_SENTENCE2 = [u'これからLEDのテストを実行します',
                      'I will now run the LED test.']

#LEDの設定
_BLACK = (0,0,0,1)
_WHITE = (1,1,1,1)
_RED = (1,0,0,1)
_GREEN = (0,1,0,1)
_BLUE = (0,0,1,1)
_CYAN = (0,1,1,1)
_MAGENTA = (1,0,1,1)
_YELLOW = (1,1,0,1)


LANG=0
def main():
    rospy.init_node('HSR_LED')
#    speaker = okdHSRLib.Speaker()
 
 #   speaker.say(_SPEECH_SENTENCE1[LANG])
  #  speaker.say(_SPEECH_SENTENCE2[LANG])
    
    led = okd_hsrlib.LED()
#    led.setColor(_RED)
    rospy.sleep(3) 
    led.setColor(_GREEN)
    #rospy.sleep(3)
#    led.setColor((1.0, 0.0, 0.0, 1.0))
#    led.setColorRGBA(R=0, G=1, B=0, A=1.0)
#    rospy.sleep(3)
    led.changeColorGradually()
    
if __name__ == "__main__":
    main()


