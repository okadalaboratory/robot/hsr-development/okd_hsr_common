#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2022 Hiroyuki Okada
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

#  * Redistributions of source code must retain the above copyright notice,
#  this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#  notice, this list of conditions and the following disclaimer in the
#  documentation and/or other materials provided with the distribution.
#  * Neither the name of Toyota Motor Corporation nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import sys
import os
import rospy
from std_msgs.msg import ColorRGBA
import actionlib
from actionlib_msgs.msg import GoalStatus
from tmc_msgs.msg import (
    TalkRequestAction,
    TalkRequestGoal,
    Voice
)

import unicodedata
def is_japanese(string):
    for ch in string:
        name = unicodedata.name(ch) 
        if "CJK UNIFIED" in name \
        or "HIRAGANA" in name \
        or "KATAKANA" in name:
            return True
    return False

class Speaker(object):
    """
    発話を完了待ちする

    HSRの使用言語が日本語の場合
    $ export LANG=ja_JP.UTF-8
    HSRの使用言語が英語の場合
    $ export LANG=en_US.UTF-8

    使用言語の確認
    $ echo $LANG
    """

    _CONNECTION_TIMEOUT = 10.0
    def __init__(self):
        talk_action = '/talk_request_action'
        self._talk_request_client = actionlib.SimpleActionClient(
            talk_action, TalkRequestAction)
 
        # Wait for connection
        try:
            if not self._talk_request_client.wait_for_server(
                    rospy.Duration(self._CONNECTION_TIMEOUT)):
                raise Exception(talk_action + ' does not exist')
        except Exception as e:
            rospy.logerr(e)
            sys.exit(1)
 
         # Detect robot's Default language
        if os.environ['LANG'] == 'ja_JP.UTF-8':
            self._lang = Voice.kJapanese
        else:
            self._lang = Voice.kEnglish
 
    def get_language(self):
        """
        使用言語の確認
        0: in Japanese, 1: in English
        """
        return self._lang

    def set_language(self, language):
        """
        使用言語の設定
        Parameters
        ----------
        language : int
        0: in Japanese, 1: in English
        """
        self._lang = language
 
    def say(self, sentence, language=None):
        """
        発話の実行
        Parameters
        ----------
        sentence : String
        発話文
        """
        goal = TalkRequestGoal()
        if language == None:
            if is_japanese(sentence):
                print("Japanese")
                goal.data.language = 0
            else:
                print("English")
                goal.data.language = 1
        else:
            print("else")
            goal.data.language = language

       # if not goal.data.language != (0 or 1:
       #     print("default")
       #     goal.data.language = self._lang
            
        goal.data.sentence = sentence
        if (self._talk_request_client.send_goal_and_wait(goal) == GoalStatus.SUCCEEDED):
            pass
        else:
            return False

class LED(object):
    """
    状態表示LEDを使う

    """
    _LED_INPUT_DATA_SIZE = 256
    _DEFAULT_COLOR = ColorRGBA(g=0.2, b=0.6)
    
    def __init__(self):
        status_led_topic = '/hsrb/command_status_led_rgb'
        self._led_pub = rospy.Publisher(status_led_topic,
                              ColorRGBA, queue_size=100)
        # Wait for connection
        while self._led_pub.get_num_connections() == 0:
            rospy.sleep(0.1)

    def changeColorGradually(self):
        rate=rospy.Rate(50)
        color = self._DEFAULT_COLOR
        for num in range(self._LED_INPUT_DATA_SIZE):
            color.r= num / float(self._LED_INPUT_DATA_SIZE -1)
            self._led_pub.publish(color)
            rate.sleep()

    def setColorRGBA(self,R=0,G=0,B=0,A=0):
        color = ColorRGBA()
        color.r = R
        color.g = G
        color.b = B
        color.a = A
        self._led_pub.publish(color)

    def setColor(self,COLOR=(0,0,0,0)):
        """
        LEDの色を変更
        Parameters
        ----------
        R : Float(0.0 - 1.0)
        G : Float(0.0 - 1.0)
        B : Float(0.0 - 1.0)
        A : Float(0.0 - 1.0)

        色のRGBA表現
        黒　BLACK (0,0,0,0) 
        白　WHITE (1,1,1,0)
        赤　RED (1,0,0,0)
        緑　GREEN (0,1,0,0)
        青　BLUE (0,0,1,0)
        シアン　CYAN(0,1,1,0)
        マゼンダ MAGENTA(1,0,1,0)
        黄　YELLOW (1,1,0,0)
       """
        color = ColorRGBA()
        print(COLOR)
        color.r = COLOR[0]
        color.g = COLOR[1]
        color.b = COLOR[2]
        color.a = COLOR[3]
        print(color)
        self._led_pub.publish(color)




