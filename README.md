# OKADA LAb. 開発HSR共通ユーティリティ
okd_hsr_common

## Document

## Install
```
mkdir -p catkin_ws/src
cd catkin_ws/src
git clone https://gitlab.com/okadalaboratory/robot/hsr-development/okd_hsr_common.git
cd okd_common
rosdep install -y -r --from-path . --ignore-src
cd ../../
catkin_make
```

## How to use

